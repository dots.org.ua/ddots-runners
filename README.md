Runners Docker images for DDOTS
================================

This repo contains Docker images with tools for testing solutions for DDOTS.

There are the following runners right now:

* binary (C/C++/FPC/other compiled languages produce binary file, which can be executed directly)
* python 2.7
* python 3.4
* java (openjdk or oracle?)
* ruby
* mono

These are going to be implemented:

* scala
* javascript
* clojure
* common lisp


HowTo build images
------------------

Makefile is prepared and it is very easy to rebuild all images:

```bash
$ make
```

If you want ot rebuild only one image:

```bash
$ make binary
```
