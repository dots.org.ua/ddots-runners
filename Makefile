DOCKER_RUNNERS := $(wildcard images/*)

OS:=$(shell uname -s)

ifeq ($(OS),Linux)
	CARGO_BUILD_JOBS ?= $(shell grep -c processor /proc/cpuinfo)
else ifeq ($(OS),Darwin)
	CARGO_BUILD_JOBS ?= $(shell sysctl -n hw.ncpu)
endif

DDOTS_NAME ?= ddots
BUILD_PATH = build-$*
COMMON_PATH = common

DOCKERFILE_PARTS = $@/Dockerfile.head $(COMMON_PATH)/Dockerfile.tail

# we target bash in echo statements, though it is not that critical
SHELL := bash

COLOR_GREEN := \033[1;32m
COLOR_WHITE := \033[1;37m
COLOR_NONE := \033[0m

define echo
	echo $$'$(COLOR_GREEN)'$1$$'$(COLOR_NONE)'
endef

.PHONY: all
all: $(DOCKER_RUNNERS)

ddots-runner/target/ddots-runner: ddots-runner/Cargo.lock ddots-runner/Cargo.toml ddots-runner/src/*
	@$(call echo,"Building ddots-runner...")
	mkdir -p /tmp/.cargo/registry
	chmod 777 /tmp/.cargo /tmp/.cargo/registry
	docker pull liuchong/rustup:stable-musl
	docker run --rm \
		--workdir /src \
		--volume `pwd`/ddots-runner:/src \
		--volume "/tmp/.cargo/registry:/root/.cargo/registry" \
		--env "CARGO_BUILD_JOBS=$(CARGO_BUILD_JOBS)" \
		rust:1.53-alpine \
		sh -c "apk add --no-cache musl-dev && cargo build --release && mv ./target/release/ddots-runner ./target/ && strip --strip-all ./target/ddots-runner && chown -R $$(id -u):$$(id -g) ./target/"
	@$(call echo,"ddots-runner has been build")

.PHONY: $(DOCKER_RUNNERS)
$(DOCKER_RUNNERS): images/%: ddots-runner/target/ddots-runner
	@$(call echo,$$'Building "$(COLOR_WHITE)$*$(COLOR_GREEN)" runner image...')
	rm -rf $(BUILD_PATH)
	mkdir $(BUILD_PATH)
	
	cp -rp $(COMMON_PATH) $(BUILD_PATH)
	cp -p ddots-runner/target/ddots-runner $(BUILD_PATH)
	
	cat $(DOCKERFILE_PARTS) > $(BUILD_PATH)/Dockerfile
	
	cd $(BUILD_PATH) && docker build --pull --tag $(DDOTS_NAME)-runner-$* .
	rm -r $(BUILD_PATH)
	@$(call echo,$$'"$(COLOR_WHITE)$*$(COLOR_GREEN)" runner image has been built')
